import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryApi implements InMemoryDbService {
    createDb() {
        let users = [
            {id: 1, name: 'Windstorm', age: 33, address: 'string', fruit: 'string'},
            {id: 2, name: 'Bombasto', age: 33, address: 'string', fruit: 'string'},
            {id: 3, name: 'Magneta', age: 33, address: 'string', fruit: 'string'},
            {id: 4, name: 'Tornado', age: 33, address: 'string', fruit: 'string'},
        ];
        return {users};
    }
}
