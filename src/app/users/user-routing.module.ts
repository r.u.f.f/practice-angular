import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {UserListComponent} from './user-list.component';
import {UserDetailComponent} from './user-detail.component';
import {NewUserComponent} from './new-user.component';

const userRoutes: Routes = [
    {path: 'users', component: UserListComponent},
    {path: 'user/:id', component: UserDetailComponent},
    {path: 'new', component: NewUserComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(userRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class UserRoutingModule {
}
