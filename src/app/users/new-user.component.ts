import 'rxjs/add/operator/switchMap';
import {Component, HostBinding} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {User, UserService} from './user.service';

@Component({
    templateUrl: './new-user.component.html'
})
export class NewUserComponent  {
    @HostBinding('style.display') display = 'block';
    @HostBinding('style.position') position = 'absolute';
    user: User;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: UserService) {
    }

    ngOnInit() {
    }

    goToUsers() {
        this.router.navigate(['/users']);
    }

    createUser(user: User) {
        this.service.newUser(user)
            .subscribe( () => this.goToUsers());
    }
}
