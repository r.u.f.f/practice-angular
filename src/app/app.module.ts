import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {PageNotFoundComponent} from './not-found.component';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {UsersModule} from './users/users.module';
import {HttpModule} from '@angular/http';

import { HttpInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryApi } from './InMemoryApi.service';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        UsersModule,
        AppRoutingModule,
        HttpInMemoryWebApiModule.forRoot(
            InMemoryApi, { dataEncapsulation: false }
        )
    ],
    declarations: [
        AppComponent,
        PageNotFoundComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
